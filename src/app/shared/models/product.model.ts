export class Product {
	
	id: number;
	name: string;
	quantity: number;

  constructor(
  	id?: number, 
  	name?: string, 
  	quantity?: number
  ) { }
}