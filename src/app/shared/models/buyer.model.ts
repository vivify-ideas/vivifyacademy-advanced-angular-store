import { Product } from './product.model';

export class Buyer {

  constructor(
    id?: number, 
    firstName?: string, 
    lastName?: string, 
    email?: string,
    products?: Product[]
  ) { }
}