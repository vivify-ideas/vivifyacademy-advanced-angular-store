import { Injectable } from '@angular/core';
import { ProductsService } from '../../shared/services/products.service';
import { HttpClient } from '@angular/common/http';
import { Buyer } from '../../shared/models/buyer.model';

@Injectable()
export class BuyersService {

  API_ENDPOINT = 'http://localhost:8000/';

	private buyers: Buyer[] = [
    {
      id: 1,
      firstName: "Nikola",  
      lastName: "Nikolic",
      email: "nikolic@gmail.com",
      products: [
        { name : 'Televizor' },
        { name : 'Mobilni' },
        { name : 'Racunar' }
      ]
    },
    {
      id: 2,
      firstName: "Milan",
      lastName: "Miovanovic",
      email: "milovanovic@gmail.com",
      products: [
        { name : 'Casa' },
        { name : 'Sto' },
        { name : 'Stolica' }
      ]
    },
    {
      id: 3,
      firstName: "Petar",
      lastName: "Petrovic",
      email: "petrovic@gmail.com",
      products: [
        { name : 'Slusalice' },
        { name : 'Mis' },
        { name : 'Tastatura' }
      ]
    },
    {
      id: 4,
      firstName: "Ivan",
      lastName: "Ivanovic",
      email: "ivanovic@gmail.com",
      products: [
        { name : 'Torba' },
        { name : 'Kesa' },
        { name : 'Patike' }
      ]
    }
  ]

  constructor(private productService: ProductsService,
              private http: HttpClient) { }

  public getBuyers() {
  	return this.http.get<Buyer[]>(this.API_ENDPOINT + 'buyers.php');
  }

  public deleteBuyer(buyer) {
  	const index = this.buyers.indexOf(buyer);
    this.buyers.splice(index, 1);
  }

  public addBuyer(newBuyer) {
    return this.http.post(this.API_ENDPOINT + 'buyers-add.php',{
      firstName: newBuyer.firstName,
      lastName: newBuyer.lastName,
      email: newBuyer.email
    });
  }

  public newBuyerId() {
    return this.buyers.length + 1;
  }

  public getBuyerById(id) {
    return this.buyers.find( buyer => {
      return buyer['id'] == id;
    })
  }

  public buyerPurchase(product, id){
    let buyer = this.getBuyerById(id);
    this.addProduct(buyer, product);   
  }

  public addProduct(buyer, product) {
     buyer.products.push(product);
     this.productService.removeQuantity(product.quantity)
  }
}
