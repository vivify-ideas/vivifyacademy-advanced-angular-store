import { Injectable } from '@angular/core';
import { Product } from '../../shared/models/product.model';

@Injectable()
export class ProductsService {

  private products: Product[] = [
    {
      id: 1,
      name: "Televizor",
      quantity: 135,
    },
    {
      id: 2,
      name: "Mobilni",
      quantity: 22,
    },
    {
      id: 3,
      name: "Sokovnik",
      quantity: 312,
    },
    {
      id: 4,
      name: "Tastature",
      quantity: 476,
    }
  ]

  constructor() { }

  public getProducts(){
    return this.products;
  }

  public addQuantity(product) {
    let singleProduct = this.findProduct(product);
    return singleProduct.quantity ++;
  }

  public removeQuantity(product) {
    let singleProduct = this.findProduct(product);
    return singleProduct.quantity --;
  }

  public findProduct(products){
    return this.products.find((product) => {
      return product['id'] === products.id;
    })
  }

  public getProductById(id) {
    return this.products.find(product => {
       return product['id'] == id;
    })
  }

}
