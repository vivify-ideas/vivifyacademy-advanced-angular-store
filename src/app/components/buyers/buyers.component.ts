import { Component, OnInit } from '@angular/core';
import { BuyersService } from '../../shared/services/buyers.service';
import { Buyer } from '../../shared/models/buyer.model';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-buyers',
  templateUrl: './buyers.component.html'
})
export class BuyersComponent implements OnInit {

	buyers: Buyer[];
  newBuyer: Buyer = new Buyer();

  constructor(private buyersService: BuyersService) {
    buyersService.getBuyers().subscribe(data =>{
      this.buyers = data;
    },
    (err: HttpErrorResponse)=>{
      alert(`Backend returned code ${err.status} with message: ${err.error}`);
    })
  }

  ngOnInit() {
  }

  public deleteBuyer(buyer) {
  	this.buyersService.deleteBuyer(buyer);
  }

  public addBuyer(newBuyer) {
    this.buyersService.addBuyer(newBuyer).
    subscribe( newBuyer => {
      this.buyers.push(newBuyer);
    },
    (err: HttpErrorResponse)=>{
      alert(`Backend returned code ${err.status} with message: ${err.error}`);
    });
    this.newBuyer = new Buyer();
  }
}
