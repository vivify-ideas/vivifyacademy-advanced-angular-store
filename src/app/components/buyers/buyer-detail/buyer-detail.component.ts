import { Component, OnInit } from '@angular/core';
import { BuyersService } from '../../../shared/services/buyers.service';
import { Buyer } from '../../../shared/models/buyer.model';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-buyer-detail',
  templateUrl: './buyer-detail.component.html'
})
export class BuyerDetailComponent implements OnInit {

	buyer: Buyer;

  constructor(private buyersService: BuyersService,
  						private route: ActivatedRoute) { }

  ngOnInit() {
  	let id = parseInt(this.route.snapshot.paramMap.get('id'));
    this.buyer = this.buyersService.getBuyerById(id);
  }

}
