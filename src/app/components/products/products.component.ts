import { Component, OnInit } from '@angular/core';
import { ProductsService } from '../../shared/services/products.service';
import { Product } from '../../shared/models/product.model';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html'
})
export class ProductsComponent implements OnInit {

  products: Array <Product>;
  filterProduct: string = '';

  constructor(private productsService: ProductsService) { }

  ngOnInit() {
    this.products = this.productsService.getProducts();
  }

  public addQuantity(product) {
    this.productsService.addQuantity(product);
  }

  public removeQuantity(product) {
    this.productsService.removeQuantity(product);
  }

}
