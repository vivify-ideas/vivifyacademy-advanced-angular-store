import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { BuyersComponent } from './components/buyers/buyers.component';
import { BuyerDetailComponent } from './components/buyers/buyer-detail/buyer-detail.component'
import { ProductsComponent } from './components/products/products.component';
import { ProductDetailComponent } from './components/products/product-detail/product-detail.component';

const appRoutes: Routes = [
  {
    path: '',
    redirectTo: '/products',
    pathMatch: 'full'
  },
  {
    path: 'products',
    component: ProductsComponent,
    children: [
      {
        path: ':id',
        component: ProductDetailComponent
      }
    ]
  },
  {
    path: 'buyers',
    component: BuyersComponent,
    children: [
      {
        path: ':id',
        component: BuyerDetailComponent
      }
    ]
  }
];

@NgModule({
  imports:[
    RouterModule.forRoot(appRoutes)
  ], 
    exports: [
        RouterModule
    ]
})

export class AppRoutingModule {}